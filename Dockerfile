FROM registry.gitlab.com/the-internets/docker/apache-php-fpm:7.4

MAINTAINER Léo Chéron <leo@cheron.works>

RUN apt update && \
	apt install curl && curl -sLO https://deb.nodesource.com/setup_12.x && bash setup_12.x && \
	apt install -y nodejs